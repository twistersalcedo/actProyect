package com.example.twist.actproyect;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class detalles extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView bnv = (BottomNavigationView)findViewById(R.id.buttom_menu);
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId())
                {
                    case R.id.facebook:
                        Toast.makeText(detalles.this,"ClicasteFacebook",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.twitter:
                        Toast.makeText(detalles.this,"Clicaste Twitter",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.youtube:
                        Toast.makeText(detalles.this,"Clicaste Youtube",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.instagram:
                        Toast.makeText(detalles.this,"Clicaste Instagram",Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
    }

}
